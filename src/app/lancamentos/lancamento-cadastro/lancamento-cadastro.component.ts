import { CoreModule } from './../../core/core.module';
import { LancamentoService, LancamentoFiltro } from './../../services/lancamento.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LancamentoModel } from './../../core/model/lancamento.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-lancamento-cadastro',
  templateUrl: './lancamento-cadastro.component.html',
  styleUrls: ['./lancamento-cadastro.component.css']
})
export class LancamentoCadastroComponent implements OnInit {

  tipos = [
    { label: 'Receita', value: 'RECEITA' },
    { label: 'Despesa', value: 'DESPESA' }
  ];

  categorias = [
    { label: 'Alimentação', value: 1 },
    { label: 'Transporte', value: 2 }
  ];

  pessoas = [
    { label: 'João da Silva', value: 1 },
    { label: 'Sérgio Stanisci', value: 2 },
    { label: 'Gabriela Batista', value: 3 }
  ];

  private lancamento = new LancamentoModel();
  private lancamentos = [];
  private error: any;

  descricao: string;
  dataVencimentoInicio: Date;
  dataVencimentoFim: Date;


  constructor(private route: ActivatedRoute,
    private router: Router,
    private service: LancamentoService,

  ) { }

  ngOnInit() {

    const codigoLancamento: number = this.route.snapshot.params['idLancamento'];

    if (codigoLancamento) {

      this.consultarById(codigoLancamento);
    }
  }

  pesquisar() {

    const filtro: LancamentoFiltro = {
      descricao: this.descricao,
      dataVencimentoInicio: this.dataVencimentoInicio,
      dataVencimentoFim: this.dataVencimentoFim
    };

    this.service.pesquisar(filtro)
      .then(lancamentos => this.lancamentos = lancamentos);
  }

  get editando(): Boolean {
    return Boolean(this.lancamento.id);
  }

  savar(form: FormControl) {
    if (this.editando) {
      this.atualizarLancamento(form)
    } else {
      this.adicionarLancamento(form);
    }
  }

  atualizarLancamento(form: FormControl) {
    this.service.atualizar(this.lancamento)
      .then(lancamento => {
        this.lancamento = lancamento;
      })
      .catch(error => this.error(error));
  }

  adicionarLancamento(form: FormControl) {
    this.service.adicionar(this.lancamento)
      .then(() => {
        // form.reset();
        // this.lancamento = new LancamentoModel();

        this.router.navigate(['/lancamentos']);
      })
      .catch(erro => this.error(erro));
  }

  private consultarById(codigoLancamento: number) {

    this.service
      .consultarById(codigoLancamento)
      .then(lancamento => {
        this.lancamento = lancamento;
      })

  }

}
