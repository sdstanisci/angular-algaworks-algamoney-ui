import { LancamentoService, LancamentoFiltro } from './../../services/lancamento.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-lancamentos-pesquisa',
  templateUrl: './lancamentos-pesquisa.component.html',
  styleUrls: ['./lancamentos-pesquisa.component.css']
})
export class LancamentosPesquisaComponent {

lancamentos = [];

descricao: string;
dataVencimentoInicio: Date;
dataVencimentoFim: Date;

constructor(private service: LancamentoService){}

  ngOnInit() {

    this.pesquisar();
  }

  pesquisar(){

    const filtro: LancamentoFiltro = {
      descricao: this.descricao,
      dataVencimentoInicio: this.dataVencimentoInicio,
      dataVencimentoFim: this.dataVencimentoFim
    };

    this.service.pesquisar(filtro)
    .then(lancamentos => {
      this.lancamentos = lancamentos;

      console.log(this.lancamentos);
    });
  }

}
