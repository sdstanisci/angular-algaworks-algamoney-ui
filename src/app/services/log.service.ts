import { Inject } from "@angular/core";

export class LogService{

    // constructor(@Inject('LogPrefixo') private prefixo: string){ } - para injetar a partir do appmodule

    log(msg: string){
        console.log(`Log: ${msg}`);
    }
}