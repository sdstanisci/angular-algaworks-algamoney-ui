import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

export interface LancamentoFiltro{
  descricao: string;
  dataVencimentoInicio: Date;
  dataVencimentoFim: Date;
}

@Injectable()
export class LancamentoService {

  lancamentosUrl = 'http://localhost:3000/lancamentos';

  constructor(private http: Http) { }

  pesquisar(filtro: LancamentoFiltro): Promise<any> {

    const params = new URLSearchParams();
    const headers = new Headers();

    headers.append('Authorization','Basic xxxxxxxxxxxxxxxxxxxxxxxxxxxxx==');

    if(filtro.descricao){
      params.set('descricao', filtro.descricao)
    }

    // *** ajustar para usar o moment
    if(filtro.dataVencimentoInicio){
      params.set('dataVencimentoInicio', filtro.dataVencimentoInicio.toString());
    }

    // *** ajustar para usar o moment
    if(filtro.dataVencimentoFim){
      params.set('dataVencimentoFim', filtro.dataVencimentoFim.toString());
    }

    // *** implentar o header
    return this.http.get(`${this.lancamentosUrl}?resumo`, { headers, search: params} )
              .toPromise()
              .then(response => {
                response.json().content();
              })
  }

  consultar(): Promise<any>{

    return this.http.get('http://localhost:3000/lancamentos')
        .toPromise()
        .then(response => response.json());
  }

  consultarById(id: number){
    return this.http.get(`http://localhost:3000/lancamentos/${id}`)
        .toPromise()
        .then(response => response.json());
  }

  adicionar(lancamento: any) :Promise<any>{
    return this.http.post('http://localhost:3000/lancamentos', lancamento)
        .toPromise()
        .then(response => response.json());
  }

  excluir(id: number): Promise<any>{
    return this.http.delete(`http://localhost:3000/lancamentos/${id}`)
        .toPromise()
        .then( response => response.json());
  }

  atualizar(lancamento: any): Promise<any>{
    return this.http.put(`http://localhost:3000/cidade/${lancamento.id}`, lancamento)
        .toPromise()
        .then( response => response.json())
        .catch(error => {
          return Promise.reject(`Erro ao alterar a cidade ${lancamento.descricao}`)
        });
  }

}
