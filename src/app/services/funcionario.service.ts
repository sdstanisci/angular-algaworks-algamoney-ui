import { Injectable } from '@angular/core';
import { LogService } from '../services/log.service';

@Injectable()
export class FuncionarioService{

    funcionarios = [
        {id: 1 , nome: 'Sérgio'}
    ];

    id: number = 1;

    constructor(private logService: LogService){}

    adicionar(nome){

        const funcionario = {
            id: ++this.id,
            nome: nome
        }

        this.funcionarios.push(funcionario);

        console.log(JSON.stringify(this.funcionarios));
    }

    consultar(){
        return this.funcionarios;
    }

}
