export class LancamentoModel {
  id?: number;
  tipo?: string;
  descricao?: string;
  dataVencimento?: string;
  dataRecebimento?: string;
  valor?: number;
  pessoa?: string;

  constructor(){}
}
