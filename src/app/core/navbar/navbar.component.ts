import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  exibindoMenu: boolean = true;
  
  statusNavBar(): boolean{
    
    return this.exibindoMenu = !this.exibindoMenu;
  }
  
}
