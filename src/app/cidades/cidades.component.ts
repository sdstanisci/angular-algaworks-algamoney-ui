import { Component, OnInit } from '@angular/core';
import { CidadesService } from '../services/cidades.service';

@Component({
  selector: 'app-cidades',
  templateUrl: './cidades.component.html',
  styleUrls: ['./cidades.component.css']
})
export class CidadesComponent implements OnInit {

  cidades = [];

  constructor(private service: CidadesService) { }

  ngOnInit() {
    this.consultar();
  }

  consultar(){
    this.service.consultar()
    .then(cidades => {
      this.cidades = cidades;
    });
  }

  adicionar(nome: string){
    this.service.adicionar( { nome } )
        .then( cidade => {
          alert( `Cidade: ${cidade.nome}, Id: ${cidade.id} Cadastrada com sucesso ! `);
          this.cidades.push(cidade);
        });
  }

  excluir(cidade: any){
    this.service.excluir(cidade.id)
        .then(retorno => {
          alert( `Cidade: ${cidade.nome} , Id: ${cidade.id} excluida com sucesso !`);
          this.consultar();
        });
  }

  atualizar(cidade: any){
    this.service.atualizar(cidade)
        .then(retorno => {
          alert( `Cidade: ${cidade.nome} , Id: ${cidade.id} atualizada com sucesso !`);
          this.consultar();
        })
        .catch(error => {
          alert(error);
        });
  }
}
