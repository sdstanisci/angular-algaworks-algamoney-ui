import { BotaoGrandeComponent } from './botoes/botao-grande/botao-grande.component';
import { Routes, RouterModule } from '@angular/router';

//Component
import { AppComponent } from './app.component';
import { CidadesComponent } from './cidades/cidades.component';
import { LancamentosPesquisaComponent } from './lancamentos/lancamentos-pesquisa/lancamentos-pesquisa.component'
import { LancamentoCadastroComponent } from './lancamentos/lancamento-cadastro/lancamento-cadastro.component'
import { PessoaCadastroComponent } from './pessoas/pessoa-cadastro/pessoa-cadastro.component'

//Module
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LancamentosModule } from "./lancamentos/lancamentos.module";
import { PessoasModule } from './pessoas/pessoas.module';
import { CoreModule } from './core/core.module';
import { FuncionarioModule } from './funcionario/funcionario.module';
import { NavegacaoModule } from './navegacao/navegacao.module';

//provider
import { LogService } from './services/log.service';
import { CidadesService } from './services/cidades.service';
import { FuncionarioService } from './services/funcionario.service';
import { LancamentoService } from './services/lancamento.service';

const routes: Routes = [
  {path: 'lancamentos', component: LancamentosPesquisaComponent},
  {path: 'lancamentos/novo', component: LancamentoCadastroComponent},
  {path: 'pessoas', component: PessoaCadastroComponent},

  {path: 'lancamentos/novo/:idLancamento', component: LancamentoCadastroComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    CidadesComponent,

 ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),

    LancamentosModule,
    PessoasModule,
    CoreModule,
    FuncionarioModule,
    NavegacaoModule,
  ],
  providers: [ LogService, CidadesService, LancamentoService], //{provide: 'LogPrefixo', useValue: 'Log'} - Para injetar parametro no construtor
  bootstrap: [AppComponent]
})
export class AppModule { }
