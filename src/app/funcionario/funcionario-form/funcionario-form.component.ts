import { Component, OnInit } from '@angular/core';
import { FuncionarioService } from '../../services/funcionario.service';
import { LogService } from '../../services/log.service';

@Component({
  selector: 'app-funcionario-form',
  templateUrl: './funcionario-form.component.html',
  styleUrls: ['./funcionario-form.component.css']
})
export class FuncionarioFormComponent implements OnInit {

    funcionarios = [];

    constructor(private funcionarioService: FuncionarioService,
                private logService: LogService){}

    ngOnInit(){
      this.consultar();
    }

    adicionar(nome: string){

      this.logService.log(`adicionando ${nome}`);

      this.funcionarioService.adicionar(nome);

    }

    consultar(){
      this.funcionarios = this.funcionarioService.consultar();
    }
}
