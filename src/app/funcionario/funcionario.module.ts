import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//component
import { FuncionarioFormComponent }  from './funcionario-form/funcionario-form.component';
import { FuncionarioCardComponent } from './funcionario-card/funcionario-card.component';

//providers
import { FuncionarioService } from '../services/funcionario.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FuncionarioFormComponent, FuncionarioCardComponent],
  
  exports: [FuncionarioFormComponent,
            FuncionarioCardComponent],
  providers: [FuncionarioService]

})
export class FuncionarioModule { }
